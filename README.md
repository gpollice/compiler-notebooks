# CS4533 & CS544 README

This information page serves both **CS4533**: *Techniques of Programming Language Translation* and **CS544**: *Compiler Construction*.

## Repository purpose

* Contains all of the Jupyter Notebook pages that will be served to the students on the WPI Jupyter Hub server. Most of the pages, up to optimization are shared by both courses. 
* Some that go more into the theory of parsing, grammars, etc. are not part of the CS4533 course due to time constraints.
* When pages are different for each course, but address the same topic, the page name is appended with a `-U` for CS4533 and `-G` for CS544.

Version: 2022

