{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0a2e5460-1e8d-4a50-84c0-e7793c1e1926",
   "metadata": {},
   "source": [
    "# Calculator Language Reference\n",
    "\n",
    "Calculator is a simple language that we use for illustrating the concepts in the course. This reference describes the syntax and semantics somewhat informally.\n",
    "\n",
    "### Syntax\n",
    "\n",
    "The next sections give a somewhat informal description of the various components of the language. We use a form of Backus-Naur (BNF)<sup>[[1]](https://www.sciencedirect.com/topics/computer-science/backus-naur-form)</sup> in parts of the description. When we get to the implementation we will use the grammar notations from ANTLR.\n",
    "\n",
    "#### Expressions\n",
    "\n",
    "Calculator programs are simply a sequences of \"expressions.\" The syntax of a program and the expressions are as follows.\n",
    "\n",
    "```\n",
    "<program> ::= <expression> ';'| <expression> ';' <program>\n",
    "\n",
    "<expression> ::= <assignment expression> \n",
    "      | <arithmetic expression>\n",
    "      | <boolean expression>\n",
    "      | '(' <expression> ')'\n",
    "\n",
    "<assignment expression> ::= <variable> <assignment operator> <expression> \n",
    "\n",
    "<arithmetic expression> ::= <binary arithmetic expression> \n",
    "      | <unary arithmetic expression>\n",
    "      | <variable>\n",
    "      | <integer>\n",
    "      | '(' <arithmetic expression> ')'\n",
    "\n",
    "<binary arithmetic expression> ::= \n",
    "      <arithmetic expression> <arithmetic operator> <arithmetic expression>\n",
    "\n",
    "<unary arithmetic expression> ::= '-' <arithmetic expression>\n",
    "\n",
    "<boolean expression> ::= <binary boolean expression>\n",
    "      | <unary boolean expression>\n",
    "      | <variable>\n",
    "      | <boolean constant>\n",
    "\n",
    "<binary boolean expression> ::= \n",
    "      <arithmetic expression> <relational operator> <arithmetic expression>\n",
    "      | <expression> '=' <expression>\n",
    "      | <expression> `~=` <expression>\n",
    "\n",
    "<unary boolean expression> ::= '~' <boolean expression>\n",
    "```\n",
    "\n",
    "#### Operators\n",
    "\n",
    "There are several operators in our grammar. Operators perform some action. We have arithmetic, relational, logical, and assignment operators.\n",
    "\n",
    "```\n",
    "<arithmetic operator> ::= + | - | * | /\n",
    "\n",
    "<relational operator> ::= < | > | = | ~=\n",
    "\n",
    "<logical operator> ::= ~\n",
    "\n",
    "<assignment operator> ::= :=\n",
    "```\n",
    "\n",
    "There are ten operators. This may not result in just ten tokens or lexemes. For example, is `~=` one (`~=`) or two (`~` and `=`) tokens? We'll take this up when we build the scanner.\n",
    "\n",
    "#### Variables and constants\n",
    "\n",
    "The simple memory model means we need to be able to access locations in our memory. We will use variables made up of a sequence of letters. For now let's assume that we know what a \"letter\" is. Our lexicon also has integers (a sequence of digits) and boolean constants.\n",
    "\n",
    "```\n",
    "<variable> ::= <letter> | <letter> <variable>\n",
    "\n",
    "<integer> ::= <digit> | <integer> <digit>\n",
    "\n",
    "<boolean constant> ::= true | false\n",
    "```\n",
    "\n",
    "Notice that I defined `variable` and `integer` differently. I did this just to show that you can use recursive definitions that are either left-recursive (integer) or right-recursive (variable). While these are conceptually equivalent they result in very different approaches to lexical analysis and scanner generators. You need to decide which approach you take and adjust your grammars accordingly to enable the tools to work.\n",
    "\n",
    "#### Punctuation\n",
    "\n",
    "There are some syntactic elements that are necessary, but do not have operational roles. They are simply separators that help recognize the other syntax items. For Calculator these are: `;`, `(`, and `)`. The semicolon is used to signal the end of an expression. \n",
    "\n",
    "Parentheses are operators used to group operations to change the order in which expressions are evaluated. This is the same as one might find in most programming languages and mathematics references.\n",
    "\n",
    "#### White space\n",
    "\n",
    "White space has no use in terms of the language syntax or semantics. The sole purpose is to enhance readability. The white space characters for Calculator are: ' ' (space), `\\n`, `\\t`, `\\r`, and `EOF`.\n",
    "\n",
    "### Semantics\n",
    "\n",
    "The next sections informally describe the semantics of a Calculator program. \n",
    "\n",
    "#### Assignment\n",
    "\n",
    "An assignment expression has the form: `v := e`, where `v` is a variable and `e` is any expression. First `e` is evaluated and the result is stored in the memory location associated with `v`. If memory for `v` is associated with a memory location before the assignment, then the value in that location is overwritten. If there is no memory location assigned to `v` then one allocated for it and the value of `e` is stored there.\n",
    "\n",
    "#### Arithmetic expressions\n",
    "\n",
    "An binary arithmetic expression has the form `e1 op e2`, where `e1` and `e2` are arithmetic expressions, integers, or variables whose value is an integer, and `op` is one of the four arithmetic operators specified by the syntax. First expression `e1` is evaluated and then `e2` is evaluated and the result is the value of applying the arithmetic operator to them.\n",
    "\n",
    "A unary arithmetic operation has the form: `-e`, where `e` is an arithmetic expression, integer, or integer-valued variable. First `e` is evaluated and then negated to obtain the result. The unary `-` associates to the right; that is `--e` is evaluated as `-(-e)`.\n",
    "\n",
    "Standard arithmetic precedence applies.\n",
    "\n",
    "#### Boolean expressions\n",
    "\n",
    "A binary boolean expression has the form: `e1 op e2`, where `e1` and `e2` are arithmetic expressions, integers, or variables whose value is an integer, and `op` is one of the four relational operators specified by the syntax. First expression `e1` is evaluated and then `e2` is evaluated and the result is the value of applying the relational operator to them. The result type is boolean.\n",
    "\n",
    "Other types of boolean expressions are either a boolean constant (`true` or `false`), a variable that have boolean values, or expressions of the form `~e`, where `e` is a boolean expression. The `~` associates to the right; that is `~~e` is evaluated as `~(~e)`.\n",
    "\n",
    "### Operator precedence\n",
    "\n",
    "The following table shows the precedence of the operators in the Calculator language along with the associativity of the operators. The low numbers have the higher precedence.\n",
    "\n",
    "| Precedence | Operators   | Associativity |\n",
    "| :---:      | :---:       | :---          |\n",
    "| 1          | `( )`       | Left          |\n",
    "| 2          | Unary `- ~` | Right         |\n",
    "| 3          | `* /`       | Left          |\n",
    "| 4          | `+ -`       | Left          |\n",
    "| 5          | `< >`       | Left          |\n",
    "| 6          | `= ~=`      | Right         |\n",
    "| 7          | `:=`        | Right         |\n",
    "\n",
    "\n",
    "![](../images/Previous.png) [Module 2 Overview](ModuleOverview.ipynb)\n",
    "<br/>\n",
    "![](../images/Next.png) [Lexical Grammars](LexicalGrammars.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b53b626c-5b68-4012-8d8a-8f620001f102",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "\\[1]: *Backus-Naur Form*, Science Direct, https://www.sciencedirect.com/topics/computer-science/backus-naur-form."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.8.9 64-bit",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.9"
  },
  "vscode": {
   "interpreter": {
    "hash": "31f2aee4e71d21fbe5cf8b01ff0e069b9275f58929596ceb00d14d90e3e16cd6"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
