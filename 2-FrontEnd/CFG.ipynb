{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Context-free Grammars\n",
    "\n",
    "When we studied lexical analysis we learned that regular expressions are sufficient for scanning the input to a language processor. Now we need to move up one level in the Chomsky hierarchy<sup>[[1]](https://en.wikipedia.org/wiki/Chomsky_hierarchy)</sup> and learn about context-free languages (CFGs) and grammars.\n",
    "\n",
    "## Why do we need more than a regular grammar?\n",
    "\n",
    "This is a good question. Our lexical grammar was able to recognize all of the possible lexemes in our input stream and create tokens for them. Isn't this good enough to decide if the input is a correct program? Actually, no! When we perform syntax analysis, we are beyond recognizing simple sequences of characters, or even tokens. We need to try to construct a \"structure\" from them. This structure represents the relationships between the tokens, not just a linear sequence.\n",
    "\n",
    "When you study formal language theory, you will prove how grammars/languages are more powerful than others. We consider \"powerful\" to mean that the grammars can be used to recognize or generate a larger set of strings.\n",
    "\n",
    "## Example of a CFG\n",
    "\n",
    "This is a classic example that illustrates the limitations of regular grammars. A regular expression can describe a sequence of   `a`<sup>`m`</sup>`b`<sup>`n`</sup> where `m ≥ 0` and `n ≥ 0`. The regular grammar would look like this:'\n",
    "\n",
    "```\n",
    "R ::=  | aR | S;\n",
    "S ::= 𝜺 | bS ;\n",
    "```\n",
    "\n",
    "Note, that `𝜺` represents the empty string.\n",
    "\n",
    "---\n",
    "\n",
    "![](../images/Challenge.jpg)\n",
    "Write the derivations of the following strings using the above regular grammar.\n",
    "\n",
    "1. `aa`\n",
    "2. `bb`\n",
    "3. `abb`\n",
    "\n",
    "Compare your derivations to [my solution](Solution2.6.ipynb).\n",
    "\n",
    "---\n",
    "\n",
    "However, there is no regular expression that can recognize or generate the language of `a`<sup>`n`</sup>`b`<sup>`n`</sup> where `n ≥ 0`. For this we need a CFG. A CFG that recognizes this pattern is:\n",
    "\n",
    "```\n",
    "S ::= 𝜺 | aSb ;\n",
    "```\n",
    "\n",
    "Let's see how we might generate the string `aaabbb` from this grammar. To do this, we show a \"derivation\" that starts with the start symbol, `S`, and ends with the desired string. The following example shows this. You should be able to infer the notation. I recommend reading the short CFG introduction at [[2]](https://www.tutorialspoint.com/automata_theory/context_free_grammar_introduction.htm).\n",
    "\n",
    "```\n",
    "S ⟶ aSb ⟶ aaSbb ⟶ aaaSbbb ⟶ aaa𝜺bbb ⟶aaabbb\n",
    "```\n",
    "\n",
    "Because we have a hierarchy, the more restrictive grammars/languages are subsets of the less restrictive ones. This means that the regular grammars are also CFGs.\n",
    "\n",
    "\n",
    "## CFG format\n",
    "\n",
    "Context-free grammar rules have a specific form: `A ::= 𝛚` where `A` is a single non-terminal (`A ∈ V`<sub>`N`</sub>) and `𝛚` is a sequence of zero or more terminals and non-terminals (`𝛚 ∈ V`<sup>`*`</sup>).\n",
    "\n",
    "When we use parser generators like ANTLR, each has its own way of representing CFGs, usually by adding in some of the regular expression notation.\n",
    "\n",
    "---\n",
    "\n",
    "![](../images/Challenge.jpg)\n",
    "\n",
    "The expression grammar that we used above recognizes sequences of `a`<sup>`m`</sup>`b`<sup>`n`</sup> where `m ≠ n`. However, it cannot recognize `a`<sup>`m`</sup>`b`<sup>`n`</sup> where `m = n`. Modify the grammar to recognize this as well as when `m ≠ n` and show the derivation of `aabb`.\n",
    "\n",
    "When you are done, compare your solution to [my solution](Solution2.7.ipynb).\n",
    "\n",
    "---\n",
    "\n",
    "## Are CFGs enough?\n",
    "\n",
    "CFGs are not the only type of grammar that one might use to describe a language, but they are the preferred one. They are able to describe most programming languages and parsing is well-defined, with many tools for working with them. However, there are many languages that cannot be described by a CFG. The next more powerful language in the Chomsky Hierarchy is the *Context-sensitive* language. See [[3]](https://www.tutorialspoint.com/what-is-context-sensitive-grammar) for an introduction to the grammar required for such a language.\n",
    "\n",
    "Not all modern programming languages &mdash; possibly most of them &mdash; have some parts of the language that are not context-free. Most of the parser generators provide some mechanisms to allow us to \"cheat\" a bit and recognize these parts correctly. We will not pursue the theory behind this, but we will use these mechanisms if needed.\n",
    "\n",
    "![](../images/Previous.png) [Parsing Overview](ParsingOverview.ipynb)\n",
    "<br/>\n",
    "![](../images/Next.png) [Parsing and the Parse Tree](ParsingAndTree.ipynb)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "[1]: *Chomsky hierarchy*, wikipedia, https://en.wikipedia.org/wiki/Chomsky_hierarchy.\n",
    "\n",
    "[2]: *Context-Free Grammar Introduction*, tutorialspoint, https://www.tutorialspoint.com/automata_theory/context_free_grammar_introduction.htm.\n",
    "\n",
    "[3] *What is Context-sensitive Grammar?*, tutorialspoint, https://www.tutorialspoint.com/what-is-context-sensitive-grammar."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.8.9 64-bit",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "name": "python",
   "version": "3.8.9"
  },
  "orig_nbformat": 4,
  "vscode": {
   "interpreter": {
    "hash": "31f2aee4e71d21fbe5cf8b01ff0e069b9275f58929596ceb00d14d90e3e16cd6"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
