{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# FIRST and FOLLOW sets and functions\n",
    "\n",
    "Once we have left-factored our grammar and have no left recursion, we're ready to perform the next two steps for building the predictive parsing tables.\n",
    "\n",
    "Here is the left-factored sample grammar:\n",
    "\n",
    "```\n",
    "     E : T X\n",
    "     X : + E | ε \n",
    "     T : ( E ) | int Y\n",
    "     Y : * T | ε \n",
    "```\n",
    "\n",
    "There are two sets that we must create in order to do predictive parsing. They are the FIRST and FOLLOW sets. These are necessary for bottom-up as well as for top-down parsing. They help decide what rule should be applied to the next input symbol. We create the sets and use them to help us choose which production to choose, based upon the next input symbol. This means that we have look ahead of 1. The two subsections that follow describe each of these sets and how to compute them.\n",
    "\n",
    "## The FIRST set and FIRST() function\n",
    "\n",
    "Let `A` be a non-terminal in the grammar and `t` a terminal. The function FIRST(A) returns the set of *terminals* that can appear at the  *start* of a string derived from A.<sup>1</sup> This is the FIRST set. Formally:\n",
    "\n",
    "``` \n",
    "    First(A) = {t | A →* t𝛃} ∪ {ε | A →* ε}\n",
    "```\n",
    "\n",
    "where →* indicates one or more derivation steps and &beta; is a sentential form, possibly empty.\n",
    "\n",
    "We use an iterative fixed-point iteration algorithm to do this. This is an algorithm where we perform operations on the elements until no more changes occur and we are at a *fixed point*.\n",
    "\n",
    "The domain of the FIRST function is \"`V ∪ ℇ`\" and the range is \"`V`<sub>`T`</sub> ` ∪ ℇ`.\"<sup>2</sup>\n",
    "\n",
    "---\n",
    "\n",
    "![](../images/Tips.png) The algorithm for computing the FIRST set is:\n",
    "\n",
    "1. For each t &isin;  V<sub>T</sub>, FIRST(t) = {t}.\n",
    "\n",
    "2. For each non-terminal, A, where A : B<sub>1</sub>B<sub>2</sub> &hellip; B<sub>n</sub> is a production rule\n",
    "  - if for some i, t &isin; FIRST(B<sub>i</sub>) and &forall; j ≤ i, &epsilon; &isin; FIRST(B<sub>j</sub>), add t to FIRST(A)\n",
    "  - if &forall;i, &epsilon; &isin; FIRST(B<sub>i</sub>) then add &epsilon; to FIRST(A). \n",
    "\n",
    "3. If A : &epsilon; is a production rule, add &epsilon; to FIRST(A).\n",
    "\n",
    "Repeat steps 2 and 3 until there are no more changes.\n",
    "\n",
    "---\n",
    "\n",
    "The above algorithm works for context-free grammars, which is what we are dealing with. If you have a context-sensitive grammar, the computation has one more step that must be taken, given any string &sigma; = X<sub>1</sub>, &hellip; X<sub>n</sub>.\n",
    "\n",
    "---\n",
    "![](../images/Challenge.jpg) Can you extend the above algorithm so that given a string &sigma; = X<sub>1</sub>, &hellip; X<sub>n</sub>, you can compute the FIRST set for &sigma;\n",
    "\n",
    "The [answer is here](Solution2.11.ipynb).\n",
    "\n",
    "---\n",
    "\n",
    "![](../images/Video.png) [Video on building the FIRST set and function](https://echo360.org/media/83dc6b26-1208-410e-b43a-4c6ed0bfd389/public)\n",
    "\n",
    "## The FOLLOW set and FOLLOW() function\n",
    "\n",
    "We can define FOLLOW(N), for a non-terminal, N, in the grammar as the set of terminals that can appear immediately to the right of N in a sentential form. That is FOLLOW(N) = {t | t &isin; V<sub>T</sub> & &exist; sentential form &alpha;At&beta;}.<sup>3</sup> \n",
    "\n",
    "---\n",
    "\n",
    "![](../images/Tips.png) To compute FOLLOW(N) for all non-terminals, N:\n",
    "\n",
    "1. If N = S, the start symbol, place `$` in Follow(N), where`$` is the input right end marker (i.e. EOF).\n",
    "\n",
    "2. If there is a production X &rarr; &alpha;N&beta;, then all non-&epsilon; members of FIRST(&beta;) are in FOLLOW(N).\n",
    "\n",
    "3. If there is a production X &rarr; &alpha;N or X &rarr; &alpha;N&beta; where FIRST(&beta;) contains &epsilon; then everything in FOLLOW(X) is in FOLLOW(N).\n",
    "\n",
    "---\n",
    "\n",
    "Now that we've gotten through the theory, let's put it to practice And build the parse table.\n",
    "\n",
    "## LL(1) parsing table\n",
    "\n",
    "For each production , A &rarr; &alpha; in the grammar:\n",
    "\n",
    "1. For each terminal, t, in FIRST(&alpha;), Add A &rarr; &alpha; to M[A, t].\n",
    "\n",
    "2. If &epsilon; is in FIRST(&alpha;), then for each terminal, t, in FOLLOW(A), Add A &rarr; &alpha; to M[A, t].\n",
    "\n",
    "3. If &epsilon; is in FIRST(&alpha;) and `$` is in FOLLOW(A), add A &rarr; &alpha; to M[A, $].\n",
    "\n",
    "![](../images/Previous.png) [Predictive Parsing](PredictiveParsing.ipynb)\n",
    "<br/>\n",
    "![](../images/Next.png) [Constructing the Predictive Parsing Table](ParseTables.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "---\n",
    "\n",
    "<sup>1</sup> We use a non-terminal in this definition. However, some sources use any sentential form, &alpha;, in place of A.\n",
    "\n",
    "<sup>2</sup> Some descriptions use `EOF` as well as &epsilon;.\n",
    "\n",
    "<sup>3</sup> Remember that &alpha; and &beta; can be empty."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.8.9 64-bit",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "name": "python",
   "version": "3.8.9"
  },
  "orig_nbformat": 4,
  "vscode": {
   "interpreter": {
    "hash": "31f2aee4e71d21fbe5cf8b01ff0e069b9275f58929596ceb00d14d90e3e16cd6"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
