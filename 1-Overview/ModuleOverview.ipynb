{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "a0785db3-f59c-4a44-a56f-914434c0c2c5",
   "metadata": {},
   "source": [
    "# Module Overview\n",
    "\n",
    "This module provides an introduction to compilers; focusing on the parts of the compiler you will implement. When you finish this module, you will be ready to jump in and start your compiler project. This notebook gives an overview of language processors.\n",
    "\n",
    "In the course overview video you saw the image below. This module offers a high-level discussion of each part; how each part works and how the pieces fit together. This is how we are structuring the compiler for this course; however, it is not the only way to structure a language processor. Any compiler will perform the actions in these phases in one way or another.\n",
    "\n",
    "![](images/structure.png)\n",
    "\n",
    "This notebook introduces the module and discusses compilers and language processors in general. Specifically, what they are, what problems they solve, the theory, and technology used to develop them.\n",
    "\n",
    "## What are language processors?\n",
    "\n",
    "The obvious answer is that a language processor is a *software program that processes a language*!! We need to look beyond the obvious. Let's focus on the two words, \"language\" and \"process.\" Intuitively we understand these. As with mathematics, we need to define our words more precisely than just relying on intuition. While there are many ways you might define the two words under consideration here, we will take a cue from Humpty Dumpty in Lewis Carroll's *Through the Looking Glass* when he told Alice: \"When I use a word, it means just what I choose it to mean &mdash; nothing more, nothing less.\"<sup>1</sup> So let's provide the meaning for our two words; these are what we mean for this course.\n",
    "\n",
    "### What is a language?\n",
    "\n",
    "First let's define what we mean by a language. This definition is pretty general, and we will use a narrower meaning when talking specifically about compilers. However, this is a reasonable definition when talking about general language processing.\n",
    "\n",
    "> A *language* is a set of symbols, rules for combining the symbols in a way that has meaning.\n",
    "\n",
    "Our definition applies mainly to written languages. Many languages are spoken and have no written form. We omit these from our discussion. The languages we want to process must have some visual, symbolic form. Also, the symbols in our languages must either have inherent meaning, or be combined with other symbols, according to the rules of the language, to have meaning.\n",
    "\n",
    "Let's use English as an example. Our initial set of symbols are the 26 letters if we ignore upper and lower case differences. We can combine these into an infinite number of letter sequences that we call *words*, as long as we have no length restrictions. Most of these words do not have meaning.\n",
    "\n",
    "According to the above definition of word, \"rgy\" is a valid word. That is, it is a sequence of letter symbols. However, this has no meaning in the English language. We can combine it with \"en\" to create the word \"energy,\" or \"zymu\" to create the word \"zymurgy.\" Both of these words have meaning (go look it up if you have to).\n",
    "\n",
    "We have rules for combining letter symbols to create words that have meaning; i.e., they must be defined in a dictionary. But English is more than just words. We want to have meaningful sentences, phrases, and so on. We need more rules &mdash; enter English grammar rules. Grammar rules where we categorize the words (noun, verb, ...) and describe the legal ways of combining these.\n",
    "\n",
    "We need a few more symbols like punctuation marks and blank characters (spaces) and we can decide if a string of letters is a valid sentence. For example, the template (rule) `noun verb noun.` tells us that a sentence is valid in English. Great! Now we can determine that \"Sarah plays rugby.\" is a valid sentence.\n",
    "\n",
    "When we have the complete set of rules for English we're done defining English, right? Sorry, but it's still not enough. Using the above rule, \"Bob sits marshmallow.\" is a valid sentence. This is a true statement if we are only concerned about the form of the sentence, but the sentence is utter nonsense; it has no valid meaning. We need more. We need to know the meaning of the words in order to make the decision as to whether a sentence is valid or not.\n",
    "\n",
    "We seem to need three things to enable us to understand (and process) a language. You will see this as we get into language processing and compilers. To give you a preview, the three things we need are:\n",
    "\n",
    "1. The symbols and the words that they form. (Lexical structure)\n",
    "2. Which sequences of words and other symbols, like punctuation, are valid. (Syntactic structure)\n",
    "3. What the words and sequences mean. (Semantics)\n",
    "\n",
    "There are many other issues that we have to deal with when attempting to understand languages, but these are the main ones. We'll leave detailed discussions of these to later.\n",
    "\n",
    "### What do we mean by processing?\n",
    "\n",
    "When you read this page, you are processing the language on the page and transforming it into thought (you are doing this, right?). This is one form of language processing. When you enter a statement in English to [Google Translate](https://translate.google.com/) for example, and ask for the equivalent in another language, like Spanish, the program processes your input and produces a Spanish equivalent. Is the translation correct and an *exact* translation? Often, it isn't exact, but it is usually equivalent in meaning.\n",
    "\n",
    "---\n",
    "\n",
    "![](../images/TryIt.png)\n",
    "Go to [Google Translate](https://translate.google.com/) and ask for the English to Spanish translation of \"Some things get translated exactly, and some don't.\" Now, there is a two arrow icon above the two panels that switch the panels and will automatically translate the Spanish back to English. Is it what you started with? If you look carefully, you will see that \"get translated\" has changed to \"translate.\" Does this have the exact meaning of the first English entry?\n",
    "\n",
    "Try entering some sentences and switch back and forth between; you don't have to do English and Spanish. This is like playing the telephone game where some things get lost in translation. How many times does it take until you achieve stasis? This example illustrates why natural languages are really hard to process. Thankfully, we are restricting the type of languages we will process in this course.\n",
    "\n",
    "---\n",
    "\n",
    "Luckily, we are going to consider \"processing\" programming languages. These are languages like we defined above that are used to represent algorithms, data structures, and other types of computation. These languages are designed to be amenable to the type of processing that we are going to learn about. They almost always have a well-defined syntax (structure) and some formal semantics that can be applied to determine if an input text is valid and what the computation does. When we process a programming language we transform it into another form that is used for a different purpose. So, the definition we will use is:\n",
    "\n",
    "> Processing a programming language means that we transform the a representation in one language (the **source**) to an equivalent representation in another language (the **target**).\n",
    "\n",
    "Some examples of programming language processors are\n",
    "\n",
    "- Compilers which usually take human readable source program and produce a form that can be executed by a computer.\n",
    "\n",
    "- Interpreters which take human readable source program and execute it line by line.\n",
    "\n",
    "- Transpilers which transform source code in one language to equivalent source code in another, possibly the same, language.\n",
    "\n",
    "These examples do not have hard and fast boundaries. For example, interpreters often create a \"compiled\" form of the program for faster execution. \n",
    "\n",
    "The techniques and tools you will learn in this course apply to the above programming language processors and many other types of software.\n",
    "\n",
    "In the next few notebooks, we will look at the different components of a compiler that are shown in the image at the beginning of this notebook.\n",
    "\n",
    "![](../images/Next.png) [Lexical Analysis](LexicalAnalysis.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5bf5b4f1-2e39-45a4-a308-4eaf50bc1c2f",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "\\[1] *Lewis Carroll: Through the Looking Glass*, The Literature Page, http://www.literaturepage.com/read/throughthelookingglass-54.html.\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.8.9 64-bit",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.9"
  },
  "vscode": {
   "interpreter": {
    "hash": "31f2aee4e71d21fbe5cf8b01ff0e069b9275f58929596ceb00d14d90e3e16cd6"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
