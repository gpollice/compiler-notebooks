{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "518f2a24-b80b-41e2-a0e9-a203eb902444",
   "metadata": {},
   "source": [
    "# Semantic Analysis\n",
    "\n",
    "Up to this point, each of the compiler phases we have looked at perform transformations from one representation of the source program to an equivalent one. We relied heavily on formal grammars for the first two phases and used less formal grammars and tools in creating the AST. We use different formalisms to perform semantic analysis, but they are no less rigorous than the formal grammars in previous phases.\n",
    "\n",
    "![](images/SemanticAnalysis.png)\n",
    "\n",
    "## What happens during semantic analysis?\n",
    "\n",
    "The semantic analysis phase is quite different from the previous phases. The input is either the parse tree or AST and the output is the same as the input (with possibly some additions or updates to the tree node contents). So, what goes on during this phase?\n",
    "\n",
    "Semantics deals with the meaning of the program, not its structure. Does this mean that we can find out what the program does? No, we cannot. If a programmer writes a program to sort strings using a particular algorithm, we cannot know this during semantic analysis. That's something for the A.I. folks to ponder. What we can check as far as meaning are things like this:\n",
    "\n",
    "- Are proper types used in the different operations in the program? For example, if `s` is a string and `n` is an integer, and the language only allows the `+` operator to work on integers, we can determine that `n + s` is invalid. This is called **type checking**. Other places this is used are ensuring that the right types are being passed as arguments to functions and returned.\n",
    "\n",
    "- Are the correct number of arguments, and the correct types passed as arguments to functions.\n",
    "\n",
    "- Do all variables and functions have types? Did the programmer try to use a variable that doesn't exist?\n",
    "\n",
    "## How does it work?\n",
    "\n",
    "Semantic analysis may be done in multiple passes over the input tree. We often do this to make our code simpler than trying to perform all semantic analysis chores in one pass. There is a tradeoff between the simplicity of the code and the time and resources required to make multiple passes. Whichever approach we take, there are several things that need to be done.\n",
    "\n",
    "Formal language theory and the grammars we have talked about thus far are great tools for describing and implementing correct lexical and syntax analysis. They cannot, however, describe the semantics. We need something else. Computer scientists, mathematicians, and linguists have developed several formal methods to describe the semantics of programming language. Each of these methods has its own notation and addresses a particular type of semantics. You can find a brief discussion of denotational, operational, and axiomatic semantics [here](https://cs.stackexchange.com/questions/105910/whats-the-difference-between-operational-denotational-and-axiomatic-semantics). [[1]](https://cs.stackexchange.com/questions/105910/whats-the-difference-between-operational-denotational-and-axiomatic-semantics) Some of these, like denotational semantics rely heavily on mathematical notation and logic. These methods are often taught as a separate course. We do not have time in this course to learn and apply these. \n",
    "\n",
    "There are several less formal, sometimes ad hoc ways to describe the semantics of programming languages that can aid us in implementing semantic analysis in our compiler. One useful approach to defining the semantics is *attribute grammars*.<sup>[[2](https://melt.cs.umn.edu/silver/tutorial/4_attribute_grammars/), [3](http://homepage.divms.uiowa.edu/~slonnegr/plf/Book/Chapter3.pdf)]</sup> \n",
    "\n",
    "Attribute grammars help us determine the information that we want to keep in our AST and how to decide whether a program, if the code is generated correctly, is semantically correct. As we visit the nodes of our tree, we visit the attributes of that node and apply *semantic functions* to these. We will visit attribute grammars more when we get to the module on semantic analysis.\n",
    "\n",
    "## The symbol table\n",
    "\n",
    "This is a good time to mention the symbol table. The symbol table is simply a data structure that is created during one or more of the previous phases of compilation. It contains entries for every named symbol (variable, function, and so on) in the input text and stores information about it. For example, every variable must have a type that is known when determining the semantics of the program and for generating code.\n",
    "\n",
    "The symbol table is often created and modified in stages as more information becomes known. The table itself may not persist as an accessible object, but the entries are usually referenced from the nodes in the tree and are, therefore, accessible. We will look in depth at symbol tables as we progress through the construction of the compiler for this course.\n",
    "\n",
    "![](../images/Previous.png) [Abstract Syntax Trees](AST.ipynb) <br/>\n",
    "![](../images/Next.png) [Code Generation](CodeGeneration.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2592d474-511f-4bdd-8ed9-db1ffccc8516",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "\\[1: *What's the diference between operational, denotation, and axiomatic semantics*, Stack Exchange, https://cs.stackexchange.com/questions/105910/whats-the-difference-between-operational-denotational-and-axiomatic-semantics.\n",
    "\n",
    "\\[2] \"Attribute Grammars\", Minnesota Extensibile Language Tools Group, https://melt.cs.umn.edu/silver/tutorial/4_attribute_grammars/.\n",
    "\n",
    "\\[3] *Chapter 3: Attribute Grammars\", http://homepage.divms.uiowa.edu/~slonnegr/plf/Book/Chapter3.pdf.\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.8.9 64-bit",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.9"
  },
  "vscode": {
   "interpreter": {
    "hash": "31f2aee4e71d21fbe5cf8b01ff0e069b9275f58929596ceb00d14d90e3e16cd6"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
