{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Symbol Table Implementation: Part 1\n",
    "\n",
    "Now that we know what we need from a symbol table, how should we implement it? There are many ways to do this. If you search some of the public repositories you will find good examples. The Dragon Book<sup>[1]</sup> has a complete example in an appendix. You should assess the needs for your design based upon the language. Our Calculator language and the WPL language that you are implementing for your project do not require an overly complex design.\n",
    "\n",
    "We need three data structures for our symbol table: the symbol, scope, and the symbol table manager. You might use different terminology (e.g. \"symbol table\"\" for scope and environment\" for the symbol table manager), but whichever terminology you use, you need these three, even or a simple table.\n",
    "\n",
    "This and the next two notebooks describe how each of the three structures are implemented and work together. There are also tests that exercise the basic functionality of each.\n",
    "\n",
    "## CMake changes\n",
    "\n",
    "We need to add or modify some files in our project. These changes are described below. We will make other changes as we progress through the symbol table implementation.\n",
    "\n",
    "You may need to modify some of these or comment out some parts initially. This will depend upon your project setup. Experiment with changes if you run into problems and/or ask for help if you run into a roadblock. CMake is a powerful tool, yet very fragile in places.\n",
    "\n",
    "#### ./cmake/Symbol.cmake\n",
    "\n",
    "Add this file to the top-level **cmake** directory.\n",
    "\n",
    "```\n",
    "# Symbol component module\n",
    "\n",
    "set (SYMBOL_DIR ${CMAKE_SOURCE_DIR}/src/symbol)\n",
    "set (SYMBOL_INCLUDE ${SYMBOL_DIR}/include)\n",
    "\n",
    "set (SYMBOL_SOURCES\n",
    "  ${SYMBOL_DIR}/Scope.cpp\n",
    "  ${SYMBOL_DIR}/STManager.cpp\n",
    ")\n",
    "```\n",
    "\n",
    "You may want to comment out the two files in SYMBOL_SOURCES until you actually add them.\n",
    "\n",
    "#### src/CMakeLists.txt\n",
    "\n",
    "Add one line to include the **symbol** directory.\n",
    "\n",
    "```\n",
    "# src listfile\n",
    "include (ANTLR)\n",
    "add_subdirectory(lexparse)\n",
    "add_subdirectory(symbol)\n",
    "...\n",
    "```\n",
    "\n",
    "#### src/symbol/CMakeLists.txt\n",
    "\n",
    "Create the **src/symbol** directory and add this file:\n",
    "\n",
    "```\n",
    "# symbol listfile\n",
    "#\n",
    "include(Symbol)\n",
    "\n",
    "add_library(sym_lib OBJECT\n",
    "  ${SYMBOL_SOURCES}\n",
    ")\n",
    "include_directories(sym_lib\n",
    "  ${SYMBOL_INCLUDE}\n",
    ")\n",
    "```\n",
    "Also create the **src/symbol/include** directory.\n",
    "\n",
    "## Create the Symbol\n",
    "\n",
    "The symbol should keep all of the attributes needed to complete the compiler phases. We map the set of attributes to a specific identifier in the source program. For the Calculator, we only need to keep track of variables and their types. Since there really is no scoping in Calculator, that is all we need, but we will design our symbol table to allow multiple, nested scopes.\n",
    "\n",
    "The symbol structure for Calculator is very simple and we will use a `struct` to specify it. This is so simple, that everything can be in the header file. Create the file **src/symbol/include/Symbol.h**.<sup>2</sup>\n",
    "\n",
    "```\n",
    "#pragma once\n",
    "#include<string>\n",
    "#include<sstream>\n",
    "\n",
    "enum SymType {INT, BOOL, UNDEFINED};\n",
    "\n",
    "struct Symbol\n",
    "{\n",
    "  std::string identifier;\n",
    "  SymType type;\n",
    "\n",
    "  Symbol(std::string id,SymType t) {\n",
    "    identifier = id;\n",
    "    type = t;\n",
    "  }\n",
    "\n",
    "  std::string toString() const {\n",
    "    std::ostringstream description;\n",
    "    std::string typeName = type == INT ? \"INT\"\n",
    "      : type == BOOL ? \"BOOL\" : \"UNDEFINED\";\n",
    "    description << '[' << identifier << \", \" << typeName << ']';\n",
    "    return description.str(); \n",
    "  }\n",
    "};\n",
    "```\n",
    "\n",
    "This is really simple. The symbol has two fields, the identifier and its type; boolean (`BOOL`), integer (`INT`), or undefined (`UNDEFINED`).<sup>3</sup>\n",
    "\n",
    "---\n",
    "\n",
    "![](../images/Question.png)\n",
    "Do we really need to keep the identifier in the `Symbol` struct?\n",
    "\n",
    "While it's not necessary, the small overhead makes some of the code easier to write and understand. It also makes for a more cohesive class. If you look at the `toString()` method, we cannot include the name of the identifier if it is not in the `Symbol` data structure.\n",
    "\n",
    "---\n",
    "\n",
    "Notice that there is a `toString()` method. I tend to put methods like this in data structures so that I can easily print out the contents in a readable fashion. This is a stylistic issue and you may choose to omit these. However, I will tell you that the ability to see what's inside of your the program has saved me a lot of time.\n",
    "\n",
    "## Test the Symbol\n",
    "\n",
    "Our Symbol is very simple, but it's still a good idea to write basic (smoke) tests so that every method is tested to ensure that it works on at least one test case.\n",
    "\n",
    "### CMake changes for tests\n",
    "\n",
    "There are a few things we need to do to add our symbol table related tests to our project. \n",
    "\n",
    "#### test/cmake/SymbolTests.cmake\n",
    "\n",
    "Just add the name of each test file to a list that has the variable name `SYMBOL_TESTS`.\n",
    "\n",
    "```\n",
    "# Identify all of the symbol tests\n",
    "set(SYMBOL_TESTS \n",
    "  symbol/symbol_tests.cpp\n",
    "  #symbol/scope_tests.cpp\n",
    "  #symbol/st_manager_tests.cpp\n",
    ")\n",
    "```\n",
    "\n",
    "I commented out the two test files that we will add in the next notebooks. Make sure that you uncomment them as necessary.\n",
    "\n",
    "#### test/CMakeLists.txt\n",
    "\n",
    "We need to include the [Symbol module](#./cmake/Symbol.cmake) that we previously added to the test **CMakeLists.txt file**. Our **test/CMakeLists.txt** file now looks like this (You may not need `sym_lib` yet).\n",
    "\n",
    "```\n",
    "# Test CMakeLists.txt file\n",
    "#\n",
    "include(Testing)\n",
    "include(ANTLR)\n",
    "include(Symbol)\n",
    "\n",
    "include(cmake/LexParseTests.cmake)\n",
    "include(cmake/SymbolTests.cmake)\n",
    "\n",
    "### Set up all tests to be run\n",
    "add_executable(\n",
    "  tests\n",
    "  ${LEXPARSE_TESTS}\n",
    "  ${SYMBOL_TESTS}\n",
    ")\n",
    "add_dependencies(tests parser_lib sym_lib)\n",
    "target_include_directories(tests PUBLIC \n",
    "  ${ANTLR_INCLUDE} ${ANTLR_GENERATED_DIR}\n",
    "  ${SYMBOL_INCLUDE}\n",
    "  )\n",
    "target_link_libraries(tests \n",
    "      PRIVATE\n",
    "      ${ANTLR_RUNTIME_LIB}\n",
    "      lexparse_lib\n",
    "      sym_lib\n",
    "      Catch2::Catch2WithMain)\n",
    "\n",
    "# Get the tests registered with CTest\n",
    "catch_discover_tests(tests)\n",
    "```\n",
    "### First Symbol test\n",
    "\n",
    "There really isn't much to test with the `Symbol` struct, but we want to make sure that it works. Here is the **test/symbol/symbol_tests.cpp** file.\n",
    "\n",
    "```\n",
    "#include <catch2/catch_test_macros.hpp>\n",
    "#include \"Symbol.h\"\n",
    "\n",
    "TEST_CASE(\"Create a Symbol\", \"[symbol]\") {\n",
    "  Symbol s(\"a\", SymType::BOOL);\n",
    "  CHECK(s.identifier == \"a\");\n",
    "  CHECK(s.type == BOOL);\n",
    "  // CHECK(\"foo\" == s.toString());\n",
    "  CHECK(\"[a, BOOL]\" == s.toString());\n",
    "}\n",
    "```\n",
    "\n",
    "Notice that there is a commented out assertion. If you uncomment this, it will fail and you can see the results of the `toString()` method. If it's what you want, you can then create an assertion to test this. This will not be practical in the other parts of the symbol table classes, but it is still a good test to have so that you can see the result of `toString()`. If you just try to write the returned string to `cout`, this does not work since CTEST captures the standard output.\n",
    "\n",
    "We're now done with the symbol. Time to create our `Scope` class.\n",
    "\n",
    "![](../images/Previous.png) [Symbol Table Overview](SymbolOverview.ipynb)\n",
    "<br/>\n",
    "![](../images/Next.png) [Symbol Table Implementation: Part 2](SymbolImplementation2.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "[1] *Compilers: Principles, Techniques, & Tools, 2nd ed.*, Aho, Lam, Sethi, and Ullman, Addison-Wesley, 2007. \"The Dragon Book\"\n",
    "\n",
    "<sup>2</sup> I have removed comments to keep things brief. The files supplied at the end of this module have the comments included.\n",
    "\n",
    "<sup>3</sup> We will see what UNDEFINED is used for when we build the symbol tables by walking the AST during our semantic analysis."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.8.9 64-bit",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "name": "python",
   "version": "3.8.9"
  },
  "orig_nbformat": 4,
  "vscode": {
   "interpreter": {
    "hash": "31f2aee4e71d21fbe5cf8b01ff0e069b9275f58929596ceb00d14d90e3e16cd6"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
